import common.*
import common.Constants.CLOSING_APP_MESSAGE
import common.Constants.ENTER_AMOUNT_OF_KIDS_MESSAGE
import common.Constants.ENTER_NAME_MESSAGE
import common.Constants.ENTER_SALARY_MESSAGE
import common.Constants.GREETING_MESSAGE
import common.Constants.INVALID_DATA_ERROR_MESSAGE
import common.Constants.SEPARATOR_MESSAGE
import common.Constants.SUCCESSFULLY_ADDED_TO_DB_MESSAGE
import kotlin.system.exitProcess

val dataBase = arrayListOf<Client>()

fun main() {
    //create new user
    val newUser = inputHandler()

    // With provided data calculate salary neto
    newUser.userSalaryNeto = UserCalculation(newUser).getNetoSalary()

    // Save Client in data base
    dataBase.add(newUser)

    // Output info using output handler
    outputHandler(dataBase)
}

/**
 * Gets data from user
 * @return Validated data in form of Client class
 */

fun inputHandler(): Client {
    var validData = false
    var userName = ""
    var userSalaryBruto = ""
    var userKids = ""

    while (!validData) {
        //Greeting user and collect info
        println(GREETING_MESSAGE)

        print(ENTER_NAME_MESSAGE)
        userName = readLine().toString().trim()

        print(ENTER_SALARY_MESSAGE)
        userSalaryBruto = readLine().toString().trim()


        print(ENTER_AMOUNT_OF_KIDS_MESSAGE)
        userKids = readLine().toString().trim()


        if (
            !validateIfNotEmpty(userName) ||
            !validateUserInput(userSalaryBruto, "Double") ||
            !validateUserInput(userKids, "Int")
        ) {
            clearScreen()
            println(INVALID_DATA_ERROR_MESSAGE)
            println()
        } else {
            validData = true
        }

    }

    return Client(userName, userSalaryBruto.toDouble(), userKids.toInt())

}

// User interface
fun outputHandler(clientData: ArrayList<Client>) {
    clearScreen()
    println(SUCCESSFULLY_ADDED_TO_DB_MESSAGE)
    println(SEPARATOR_MESSAGE)

    System.out.printf(
        "%-4s%-15s%-10s%-15s%-15s\n",
        "ID",
        "NAME",
        "Kids",
        "Salary bruto",
        "Salary neto"

    )

    clientData.forEachIndexed { index, client ->
        System.out.printf(
            "%-4s%-15s%-10s%-15s%-15s\n",
            index,
            client.userName,
            client.userKids,
            client.userSalaryBruto,
            client.userSalaryNeto
        )
    }

    println(SEPARATOR_MESSAGE)
    println(Constants.ADD_NEW_MESSAGE)
    if (readLine().toString().trim() == "yes") {
        main()
    } else {
        closeApp()
    }
}

/**
 * Terminates app
 */
fun closeApp() {
    clearScreen()
    println(CLOSING_APP_MESSAGE)
    exitProcess(0)

}