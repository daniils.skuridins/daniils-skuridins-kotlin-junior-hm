package common

object Constants {

    const val GREETING_MESSAGE = "Greeting, please provide following information"
    const val ENTER_NAME_MESSAGE = "Your first name: "
    const val ENTER_SALARY_MESSAGE = "Your bruto salary: "
    const val ENTER_AMOUNT_OF_KIDS_MESSAGE = "How many kids you have? "
    const val CLOSING_APP_MESSAGE = "App Terminated"

    const val ADD_NEW_MESSAGE = "If You would like to add new client, type 'yes'. To quit app press any key!"
    const val INVALID_DATA_ERROR_MESSAGE = "All inputs are required!"
    const val SUCCESSFULLY_ADDED_TO_DB_MESSAGE = "Your data has been successfully added to our data base:"

    const val SEPARATOR_MESSAGE =
        "-------------------------------------------------------------------------------------------------------------------------"


}