package common

data class Client(
    var userName: String,
    var userSalaryBruto: Double,
    var userKids: Int,
    var socialTaxPercentage: Double = 0.11,
    var dependentPrivilege: Int = 250,
    var userSalaryNeto: Double? = null

)