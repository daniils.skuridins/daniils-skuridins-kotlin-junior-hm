package common

import java.lang.NumberFormatException

/**
 * Clears Terminal screen
 */
fun clearScreen() {
    for (i in 1..35) {
        println("")
    }
}

/**
 * Takes [userInput]
 * @return Boolean if is/ is not empty
 */
fun validateIfNotEmpty(userInput: String): Boolean {
    return userInput != ""
}

/**
 * Validates if Users provided [userInput] is Int
 * @return boolean
 */
fun validateIfInt(userInput: String): Boolean {
    return try {
        userInput.toInt()
        true
    } catch (e: NumberFormatException) {
        false
    }
}

/**
 * Validates if Users provided [userInput] is Double
 * @return boolean
 */
fun validateIfDouble(userInput: String): Boolean {
    return try {
        userInput.toDouble()
        true
    } catch (e: NumberFormatException) {
        false
    }
}

/**
 * Takes [userInput] & [requiredType] of user input
 * @return boolean representing if data is correct
 */
fun validateUserInput(userInput: String, requiredType: String): Boolean {
    // Check if NOT empty
    if (!validateIfNotEmpty(userInput)) {
        return false
    }

    when (requiredType) {
        "Int" -> {
            if (!validateIfInt(userInput)) {
                return false
            }

            if (userInput.toInt() < 0) {
                return false
            }

        }
        "Double" -> {
            if (!validateIfDouble(userInput)) {
                return false
            }
            if (userInput.toDouble() < 0) {
                return false
            }
        }
    }
    return true
}