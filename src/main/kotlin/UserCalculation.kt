import common.Client
import java.math.RoundingMode
import java.text.DecimalFormat

class UserCalculation(private var ourClient: Client) {

    /**
     * Takes Clients Salary bruto
     * calculates Income Tax based on his salary
     * @return PERCENTAGE level of income
     */
    fun calculateIncomeTax(): Double {
        val months: Int = 12
        var firstLevelOfIncomeTax: Double = 0.20
        var secondLevelOfIncomeTax: Double = 0.23
        var thirdLevelOfIncomeTax: Double = 0.314

        if (ourClient.userSalaryBruto * months <= 20004) {
            return firstLevelOfIncomeTax
        }
        if ((ourClient.userSalaryBruto * months >= 20004) && (ourClient.userSalaryBruto * 12 <= 62800)) {
            return secondLevelOfIncomeTax
        } else {
            return thirdLevelOfIncomeTax
        }
    }

    /**
     * Takes Clients salary bruto and social tax percentage
     * @return social tax
     */
    fun calculateSocialTax(): Double {
        return (ourClient.userSalaryBruto * ourClient.socialTaxPercentage)
    }

    /**
     * Takes Clients Salary bruto
     * @return Taxable Money
     */
    fun calculateTaxableMoney(): Double {
        var taxableMoney =
            (ourClient.userSalaryBruto - calculateSocialTax() - (ourClient.dependentPrivilege * ourClient.userKids))

        /**
         * if taxableMoney is equal to or smaller than 0
         * return 0 so it won't ruin calculations
         */
        if (taxableMoney >= 0) {
            return taxableMoney
        } else {
            return 0.0
        }
    }

    /**
     * Takes Clients Salary bruto
     * calculates Personal income tax
     */
    fun calculatePersonalIncomeTax(): Double {
        return (calculateTaxableMoney() * calculateIncomeTax())
    }

    /**
     * Takes Clients Salary bruto
     * calculates Neto salary
     */
    fun getNetoSalary(): Double? {
        var current : Double = (ourClient.userSalaryBruto - calculateSocialTax() - calculatePersonalIncomeTax())

        /**
         * Rounds number of Double type to 2 decimal
         */
        fun roundOffDecimal(number: Double): Double? {
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            return df.format(number).toDouble()
        }
        return roundOffDecimal(current)

    }
}
